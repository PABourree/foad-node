const router = require('express').router();
const User = require('../models/User');
const verify = require('./verifyToken');

router.get('/, verify', (req,res) => {
res.send(req.user);
User.findbyOne({_id: req.user})
});
module.exports = router;