const router = require('express').Router();
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerValidation, loginValidation} = require('./validation');




router.post('/register', async (req, res) => {

    //Lets validate the data before we add a User
    const { error } = registerValidation(req.body);
    if (!error){
        error = "NaN";
    res.json({e: error , v: value});
    };
// check si l'user existe déja
const emailExist = await User.findOne({email: req.body.email});
if (emailExist) return res.status(400).send('email existe déja');

//HASH le mdp

const salt = await bcrypt.gentSalt(10);
const hashedPassword = await bcrypt.hash(req.body.password, salt);

   
// create new user
   const user = new User({
   name: req.body.name,
   email: req.body.email,
    password: hashedPassword
   });
   try{
   const savedUser = await user.save();
     res.send({user: user._id});
    }catch(err){
     res.status(400).send(err);
    }
});

//login

router.post('/login', async (req,res) => {
//Lets validate the data before we add a User
const { error } = loginValidation(req.body);
if (!error){
    error = "NaN";
res.json({e: error , v: value});
};
// check si l'user existe déja
const user = await User.findOne({email: req.body.email});
if (!user) return res.status(400).send('email existe pas');
//si le mdp est correct
const validPass = await bcrypt.compare(req.body.password, user.password);
if(!validPass) return res.status(400).send('mpd invalide')

//create and assing a token
const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
res.header('auth-token',token).send(token);


res.send('connecté');
});
module.exports = router;