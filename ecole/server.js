const express = require('express');
const app = express();
const dotenv = require('dotenv');
var mongoose = require('mongoose');

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: true }));

//import ROutes

const authRoute = require('./routes/auth');
const postRoute = require('./routes/posts');

//Connexion BDD

dotenv.config();
mongoose.connect('process.env.DB_CONNECT', { useNewUrlParser: true }, () => console.log('connected to db!'));

//middleware

app.use(express.urlencoded({extended: false}));

//route middlewares

app.use('/api/user', authRoute);
app.use('/api/posts', postRoutes);

app.listen(27017, () => console.log('Server up and running'));


//Routes

var routeEleves = require('./routes/eleve'); //Middleware
app.use('/eleves', routeEleves);


app.get('/', (req, res) => { res.send('Hello world'); } );
/*app.get('/', function (req, res) {
    res.send('Hello world !');
});*/




app.listen(3000, () => console.log('En écoute'));